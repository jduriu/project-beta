import React, {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';

const SaleRecordForm = () => {

    const [formValues, setFormValues] = useState(
        {
            automobile: "",
            salesPerson: "",
            potentialCustomer: "",
            salesPrice: "",
        }
    );
    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            automobile: "",
            salesPerson: "",
            potentialCustomer: "",
            salesPrice: "",
        }))
    }
    const handleAutomobileInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            automobile: event.target.value,
        }));
    };

    const handleSalesPersonInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            salesPerson: event.target.value,
        }));
    };

    const handlePotentialCustomerInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            potentialCustomer: event.target.value,
        }));
    };
    const handleSalesPriceInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            salesPrice: event.target.value,
        }));
    };
    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...formValues}

        data.sales_person = data.salesPerson
        delete data.salesPerson

        data.potential_customer = data.potentialCustomer
        delete data.potentialCustomer

        data.sales_price = data.salesPrice
        delete data.salesPrice


        const saleRecordUrl = 'http://localhost:8090/api/sale_record/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(saleRecordUrl, fetchConfig);
            if (response.ok) {
                setSubmitted(true);
                resetForm();
            }
        } catch (error) {
            console.log("error", error);
        }

    }

    const[automobiles, setAutomobiles] = useState([])
    useEffect(() => {
        const url = 'http://localhost:8090/api/unsold_automobiles/';

        const fetchAutomobiles = async () => {
            try {
                const response = await fetch(url);
                const data = await response.json();
                setAutomobiles(data.automobiles)

            } catch (error) {
                console.log("error", error);
            }
        };

        fetchAutomobiles();

    }, []);

    const[salesPersons, setSalesPerson] = useState([])
    useEffect(() => {
        const Saleurl = 'http://localhost:8090/api/sales_person/';

        const fetchSalesPerson = async () => {
            try {
                const response = await fetch(Saleurl);
                const data = await response.json();
                setSalesPerson(data.sales_person)

            } catch (error) {
                console.log("error", error);
            }
        };

        fetchSalesPerson();

    }, []);

    const[potentialCustomers, setPotentialCustomer] = useState([])
    useEffect(() => {
        const Saleurl = 'http://localhost:8090/api/potential_customer/';

        const fetchPotentialCustomer = async () => {
            try {
                const response = await fetch(Saleurl);
                const data = await response.json();
                setPotentialCustomer(data.potential_customer)

            } catch (error) {
                console.log("error", error);
            }
        };

        fetchPotentialCustomer();

    }, []);

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                        <NavLink to="/sale_record">
                            <div className="alert alert-success">Sale Record created successfully!</div>
                        </NavLink>
                    </div>
                    : null}
                        <h1>Create Sale Record</h1>
                        <form onSubmit={handleSubmit} id="create-salerecord-form">
                        <div className="mb-3">
                                <select onChange={handleAutomobileInputChange} required id="automobile"
                                    name="automobile" className="form-select" value={formValues.automobile}>
                                <option value="">Choose an automobile</option>
                                    {automobiles.map(automobile => {
                                        return (
                                            <option key={automobile.vin} value={automobile.vin}>
                                                {automobile.vin}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleSalesPersonInputChange} required id="sales_person"
                                    name="sales_person" className="form-select" value={formValues.salesPerson}>
                                <option value="">Choose a sales person</option>
                                    {salesPersons.map(sales_person => {
                                        return (
                                            <option key={sales_person.employee_number} value={sales_person.employee_number}>
                                                {sales_person.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="mb-3">
                                <select onChange={handlePotentialCustomerInputChange} required id="potential_customer"
                                    name="potential_customer" className="form-select" value={formValues.potentialCustomer}>
                                <option value="">Choose a customer </option>
                                    {potentialCustomers.map(potential_customer => {
                                        return (
                                            <option key={potential_customer.id} value={potential_customer.id}>
                                                {potential_customer.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handleSalesPriceInputChange} placeholder="Sales Price" name="sales_price" id="sales_price" className="form-control" value={formValues.salesPrice} />
                                <label htmlFor="sales_price">Sales price</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    };


export default SaleRecordForm;
