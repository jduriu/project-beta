import React, {useEffect, useState} from 'react';
import { NavLink } from 'react-router-dom';

const AutomobileForm = () => {

    const [formValues, setFormValues] = useState(
        {
            color: "",
            year: "",
            vin: "",
            model: "",
        }
    );

    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            color: "",
            year: "",
            vin: "",
            model: "",
        }))
    }

    const handleColorInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            color: event.target.value,
        }));
    };
    const handleYearInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            year: event.target.value,
        }));
    };

    const handleVinInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            vin: event.target.value,
        }));
    };
    const handleModelInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            model: event.target.value,
        }));
    };
    const [submitted, setSubmitted] = useState(false);


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...formValues}
        data.model_id = data.model
        delete data.model

        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(automobileUrl, fetchConfig);
            if (response.ok) {
                setSubmitted(true);
                resetForm();
            }
        } catch (error) {
            console.log("error", error);
        }

    }

    const[models, setModels] = useState([])
    useEffect(() => {
        const url = 'http://localhost:8100/api/models/';

        const fetchModels = async () => {
            try {
                const response = await fetch(url);
                const data = await response.json();
                setModels(data.models)

            } catch (error) {
                console.log("error", error);
            }
        };

        fetchModels();

    }, []);

        return (
            <div className="row">
                <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                        <NavLink to="/automobiles">
                            <div className="alert alert-success">Automobile created successfully!</div>
                        </NavLink>
                    </div>
                    : null}
                    <h1>Create Automobile</h1>
                        <form onSubmit={handleSubmit} id="create-automobile-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleColorInputChange} placeholder="color" required
                                    type="text" name="color" id="color"
                                    className="form-control" value={formValues.color}/>
                                <label htmlFor="Color">Color</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handleYearInputChange} value={formValues.year} placeholder="year" name="year" id="year" className="form-control" />
                                <label htmlFor="year">Year</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handleVinInputChange} value={formValues.vin} placeholder="vin" name="vin" id="vin" className="form-control" />
                                <label htmlFor="vin">Vin</label>
                            </div>
                            <div className="mb-3">
                                <select onChange={handleModelInputChange} required id="model"
                                    name="model" className="form-select" value={formValues.model}>
                                <option value="">Choose a Model</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>
                                                {model.name}
                                            </option>
                                        );
                                    })}
                                </select>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    };

export default AutomobileForm;
