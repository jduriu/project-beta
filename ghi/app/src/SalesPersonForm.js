import React, {useState} from 'react';

const SalesPersonForm = () => {

    const [formValues, setFormValues] = useState(
        {
            name: "",
            employeeNumber: "",
        }
    );
    function resetForm() {
        setFormValues((entries) => ({
            ...entries,
            name: "",
            employeeNumber: "",
        }))
    }
    const handleNameInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            name: event.target.value,
        }));
    };

    const handleEmployeeNumberInputChange = (event) => {
        setSubmitted(false);
        setFormValues((entries) => ({
            ...entries,
            employeeNumber: event.target.value,
        }));
    };

    const [submitted, setSubmitted] = useState(false);

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {...formValues}
        data.employee_number = data.employeeNumber
        delete data.employeeNumber

        const salesPersonUrl = 'http://localhost:8090/api/sales_person/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            const response = await fetch(salesPersonUrl, fetchConfig);
            if (response.ok) {
                setSubmitted(true);
                resetForm();
            }
        } catch (error) {
            console.log("error", error);
        }

    }

        return (
            <div className="row">
                <div className="offset-3 col-6">
                    <div className="shadow p-4 mt-4">
                    {submitted ?
                    <div className="py-2">
                            <div className="alert alert-success">Sales representative created successfully!</div>
                    </div>
                    : null}
                        <h1>Create Sales Representative</h1>
                        <form onSubmit={handleSubmit} id="create-location-form">
                            <div className="form-floating mb-3">
                                <input onChange={handleNameInputChange} placeholder="name" required
                                    type="text" name="name" id="name"
                                    className="form-control" value={formValues.name}/>
                                <label htmlFor="Name">Name</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="text" onChange={handleEmployeeNumberInputChange} placeholder="Employee Number" name="employeeNumber" id="employeeNumber" className="form-control" value={formValues.employeeNumber} />
                                <label htmlFor="employeeNumber">Employee Number</label>
                            </div>
                            <button className="btn btn-primary">Create</button>
                        </form>
                    </div>
                </div>
            </div>
        )
    };


export default SalesPersonForm;
