from django.urls import path

from .api_views import (
    api_list_technicians,
    api_show_technician,
    api_list_appointments,
    api_show_appointment,
    api_list_automobileVOs,
    api_show_automobileVO,
)

urlpatterns = [
    path("technicians/", api_list_technicians, name="api_create_technician"),
    path("technicians/<int:pk>/", api_show_technician, name="appointment_detail"),
    path("appointments/", api_list_appointments, name="api_create_appointment" ),
    path("appointments/<str:current>/", api_list_appointments, name="api_list_not_completed_appointments"),
    path("appointment/<int:pk>/", api_show_appointment, name="show_appointment"),
    path("automobiles/<str:automobile_vo_vin>/appointments",api_list_appointments, name="api_list_"),
    path("automobileVOs/", api_list_automobileVOs, name="api_list_automobileVOs"),
    path("automobileVOs/<int:pk>/", api_show_automobileVO, name="api_automobileVO_detail"),
]
